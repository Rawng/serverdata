// Evol scripts.
// Authors:
//    4144
//    Reid
// Description:
//    A box with clothes for new players.
// Variable:
//    ShipQuests_Arpan
// Values:
//    1   Talked to Arpan and needs to get clothes.
//    2   Has the clothes.

000-2-1,51,37,0	script	Chest	NPC_CHEST_BIG,{
    .@q = getq(ShipQuests_Arpan);

    if (.@q == 0) goto L_Talk;
    if (.dir == 4) goto L_Give;

    .dir = 2;
    initnpctimer;
    startnpctimer;
    close;

L_Give:
    if (.@q > 1) goto L_Quit;
    inventoryplace CreasedShirt, 2;

    setq ShipQuests_Arpan, 2;
    getitem CreasedShirt, 1;
    getitem CreasedShorts, 1;
    npctalk3 l("You take the clothes from the chest.");
    goto L_Quit;

OnTimer220:
    stopnpctimer;
    if (.dir == 2) .dir = 4;
    if (.dir == 6) .dir = 0;
    end;

L_Talk:
    npctalk3 l("You should talk to Magic Arpan first.");

    close;

L_Quit:
    .dir = 6;
    initnpctimer;
    startnpctimer;
    close;

OnInit:
    .distance = 2;
    end;
}

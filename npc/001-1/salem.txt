// Evol scripts.
// Author:
//    Travolta
// Description:
//    Trader on the market. One of his pious escaped and
//    flies around the town.
// Variables:
//    ArtisQuests_CatchPiou   -- quest variable
// Quest states:
//    0  -- not started
//    1  -- trader asked to catch piou
//    2  -- quest finished
// Note: this script is ugly like hell

001-1,57,118,0	script	Salem#001-1	NPC_SALEM,{

    function BuyPiou {
        .@price = getarg(0);
        mesq l("The piou costs @@ E.", .@price);
        next;
        menuint l("Alright, I'll take one."), 1,
                l("I changed my mind."), 2;

        mes "";
        mesn;
        if (@menuret == 2)
            return 4;
        if (Zeny < .@price)
        {
            mesq l("Don't you try to cheat an old merchant! You don't have enough money, you need @@ E.", .@price);
            next;

            return 1;
        }
        if (!checkweight(PiouEgg, 1))
        {
            mesq l("You can't carry another @@? What a shame.", PiouEgg);
            next;

            return 2;
        }
        getinventorylist;
        if (.@inventorylist_count >= 100)
        {
            mesq l("You don't have enough room in your backpack for a @@. Go stow some of your junk and come back.", PiouEgg);
            next;

            return 3;
        }
        Zeny = Zeny - .@price;
        getitem PiouEgg, 1;
        ArtisQuests_PiousBought += 1;
        mesq l("You take good care of your piou. Remember to feed it every day.");

        return 0;
    }

//    if (2 == select("[debug]continue quest:[debug] start over"))
//    {
//        debugmes "Starting quest CatchPiou over";
//        setq ArtisQuests_CatchPiou, 0;
//    }

    .@q = getq(ArtisQuests_CatchPiou);
    @ArtisQuests_PiouPrice = 30000;

    if (.@q == 1)
    {
        if (.PiouCaught)
        {
        speech 0,
            l("Look who is back..."),
            l("And with my piou. That's wonderful. I can only imagine how hard that little bugger was to catch!"),
            l("Great, and as I promised, I give you a 90% discount on the @@ of your choice.", getitemlink (PiouEgg));
            next;

            @ArtisQuests_PiouPrice = 3000;
            ArtisQuests_PiouDiscount = 1;
            donpcevent strnpcinfo(3) + "::OnPiouFlee";
            setq ArtisQuests_CatchPiou, 2;
            .@q = getq(ArtisQuests_CatchPiou);

            .LastPiouHunter$ = "";
            ArtisQuests_CatchPiou_Difficulcy = 0;
            .@BoughtPiou = BuyPiou(@ArtisQuests_PiouPrice);
            if (.@BoughtPiou == 0)
                ArtisQuests_PiouDiscount = 0;
            else if (.@BoughtPiou == 4)
                speech 1, l("See you next time!");

            close;
        }
        else
        {
            speech 0,
                l("Look who is back..."),
                l("So, where is my piou? You should not keep an old man waiting. Go and catch it like you said you would.");
                next;
            goto L_SalemMenuShop;
        }
    }

    speech 4,
        l("Good day, my friend, come closer, come closer!");
        l("Just look at my goods for sale! Fresh fruits and vegetables were shipped only this morning. And for reasonable price, of course.");

L_SalemMenuShop:
    .@fexil = getq(ArtisQuests_Fexil);

    menuint
        l("What about those pious? They look so cute."), 0,
        l("Let's trade."), 1,
        rif(.@q != 0, l("I'd like to buy a piou.")), 2,
        rif(.@fexil == 1, l("I'm looking for somebody named Fexil.")), 3,
        menuaction(l("Quit")), 4;

    switch (@menuret)
    {
        case 0:
            goto L_AboutPious;
            break;
        case 1:
            closedialog;
            shop "Store#General001-1";
            close;
        case 2:
            if (.@q == 2 && ArtisQuests_PiouDiscount)
            {
                speech 5,
                    l("You still have a 90% discount on one piou.");
                @ArtisQuests_PiouPrice = 3000;
            }
            if (!BuyPiou(@ArtisQuests_PiouPrice))
                ArtisQuests_PiouDiscount = 0;
            break;
        case 3:
            speech 5,
                l("I think that I've seen this weakling wandering around the beach south from here this morning.");
            break;
        case 5:
            break;
    }

    closedialog;
    goodbye;
    close;

L_AboutPious:
    speech 5,
        l("Ah, the locals like keeping them as pets.");
        l("With proper training, a piou can become a good friend and faithful companion in your adventures.");

    if (getq(ArtisQuests_CatchPiou) >= 2)
    {
        goto L_SalemMenuShop;
    }

    mesq l("Their price is usually high, but you know what? I have a bargain offer for you.");
    next;
    mesq l("These little pious here can't fly. At least that's what I thought.");
    next;
    mesq l("One of these little boogers started flying early, and escaped from me.");
    next;
    mesq l("It's still too young to fly too far away, so it just circles nearby.");
    next;
    mesq l("But I can't leave to catch it, so I'm asking you.");
    next;
    mesq l("If you catch the escaped piou and bring it back, I will give you a 90% discount on a piou.");
    next;

    menuint rif (.@q == 0, l("Sounds like a good deal to me. I'll do it.")), 0,
            rif (.@q == 1, l("I'm on my way! I will bring it back to you.")), 1,
            l("I don't really have time to go chasing pious, let me just buy one at the regular price (30000 E)."), 2,
            l("I don't want to buy a piou right now."), 3;

    mes "";
    mesn;

    switch (@menuret)
    {
        case 0:
            goto L_QuestInfo;
        case 1:
            break;
        case 2:
            BuyPiou @ArtisQuests_PiouPrice;
            break;
        case 3:
            break;
    }
    closedialog;
    goodbye;
    close;

L_QuestInfo:
    mesq l("The little piou is flying nearby, all you need is to catch it and bring back to me.");
    next;
    setq ArtisQuests_CatchPiou, 1;
    ArtisQuests_CatchPiou_Difficulcy = 2;
    mesq l("Good luck!");
    close;

OnPiouFlee:
    sleep 120000;
    if (!.PiouCaught)
        end;
    .PiouCaught = 0;
    .@piou$ = "#FlyingPiou1";
    if (getstrlen(.LastPiouHunter$) > 0)
    {
        message .LastPiouHunter$, .PiouEscapedMessage$;
        .LastPiouHunter$ = "";
    }
    // debugmes "The piou escaped from " + .LastPiouHunter$;
    sleep 60000;
    enablenpc .@piou$;
    movenpc .@piou$, 53, 117;
    donpcevent .@piou$ + "::OnInit";
    end;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}
